#include <iostream>
#include <list>
using namespace std;

int main()
{
	list <int> game; //Create list name game
	list<int>::iterator tmp; //Temporarily for erase that iterator point
	list<int>::iterator it; //To point the list
	int player, pass; //Store number of player(s) and pass(es)
	
	cout << "This is Hot Potato game!" << endl;
	cout << "How many player(s) : ";
		cin >> player;
	cout << "How many pass(es) : ";
		cin >> pass;
	cout << endl;

	for (int i = 1; i <= player; i++) //Store player to the list
	{
		game.push_back(i);
	}
	
	it = game.begin(); //Making iterator pointing to first of the list

	while (game.front() != game.back()) { //Looping for circle game play
		for (int i = 1; i <= pass; i++) //Passing the hot potato each player
		{
			if (*it == game.back()) //if iterator pointing to back of the list
				it = game.begin(); //Making iterator pointing to first of the list
			else
				it++; //if iterator not pointing to back of the list. Make iterator pointing to next list
		tmp = it; //Store temporarily that iterator point
		}
		
		cout << "Player " << *tmp << " was eliminated." << endl; //Show message who is eliminated
		
		//Making this to protect iterator pass out of scope
		if (*it == game.back())
			it = game.begin();
		else
			it++;

		game.erase(tmp); //Delete the list that was was eliminated.
	}
	
	cout << endl;
	cout << "Congrats!! player " << game.front() << " is winner!" << endl; //Show message who win.
	
	system("pause");
	return 0;
}
